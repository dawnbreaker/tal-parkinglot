const mongoose = require('mongoose');

const myschema = mongoose.Schema(
	{
		parkingNo: Number,
		isAvailable: Boolean,
		isReserved: Boolean,
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model('Slot', myschema);
