const mongoose = require('mongoose');

const myschema = mongoose.Schema(
	{
		phone: String,
		firstName: String,
		lastName: String,
		gender: String,
		isPregnant: Boolean,
		isDiffabled: Boolean,
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model('User', myschema);
