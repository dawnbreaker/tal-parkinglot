const mongoose = require('mongoose');

const myschema = mongoose.Schema(
	{
		parkingNo: Number,
		userPhone: String,
		bookTime: Date,
		arrivalTime: Date,
		hasArrived: Boolean,
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model('Booking', myschema);
