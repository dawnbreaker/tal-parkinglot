const moment = require('moment');

const UserModel = require('../models/user.model');
const SlotModel = require('../models/slot.model');
const BookingModel = require('../models/booking.model');

exports.listAvailableSlots = async (req, res) => {
	let availableSlots = await SlotModel.find({ isAvailable: true });
	return res.status(200).json(availableSlots);
};

exports.listOccupiedSlots = async (req, res) => {
	let occupiedSlots = await SlotModel.find({ isAvailable: false });
	return res.status(200).json(occupiedSlots);
};

exports.listRegisteredUsers = async (req, res) => {
	let regUsers = await UserModel.find();
	return res.status(200).json(regUsers);
};

exports.registerParkingSlot = async (req, res) => {
	//check if the user with this phone is already in the database
	//if not, create one

	let doc = await UserModel.updateOne(
		{ phone: req.body.phone },
		{
			phone: req.body.phone,
			firstName: req.body.firstName,
			lastName: req.body.lastName,
			gender: req.body.gender,
			isPregnant: req.body.isPregnant,
			isDiffabled: req.body.isDiffabled,
		},
		{ upsert: true }
	);

	//check if the user is eligible for reserved slot
	let reserveStatus = false;
	if (
		(req.body.gender === 'Female' && req.body.isPregnant === true) ||
		req.body.isDiffabled === true
	) {
		reserveStatus = true;
	}
	let msg = null;

	const checkReservedParking = async () => {
		let slot = 0;
		for (i = 1; i <= 24; i++) {
			let doc1 = await SlotModel.findOne({ parkingNo: i });
			if (doc1.isAvailable) {
				slot = i;
				break;
			}
		}
		return slot;
	};

	const checkGeneralParking = async () => {
		let slot = 0;
		for (i = 25; i <= 120; i++) {
			let doc1 = await SlotModel.findOne({ parkingNo: i });
			if (doc1.isAvailable) {
				slot = i;
				break;
			}
		}
		return slot;
	};

	const bookParking = async (x) => {
		//check if capacity is more than 50%
		let availableSlots = await SlotModel.find({ isAvailable: true });
		let waitTime = availableSlots.length >= 60 ? 15 : 30;
		let doc2 = await BookingModel.updateOne(
			{ userPhone: req.body.phone },
			{
				parkingNo: x,
				userPhone: req.body.phone,
				bookTime: new Date(),
				arrivalTime: moment(new Date()).add(waitTime, 'm').toDate(),
				hasArrived: false,
			},
			{ upsert: true }
		);
		let doc3 = await SlotModel.findOneAndUpdate(
			{ parkingNo: x },
			{ isAvailable: false }
		);
	};

	if (reserveStatus) {
		let slot = await checkReservedParking();
		if (slot !== 0) {
			//book reserved parking for reserved user
			let result = await bookParking(slot);
			msg = { msg: 'Reserved Parking Booked', parkingNo: slot };
		} else {
			//book general parking for reserved user
			slot = await checkGeneralParking();
			let result = await bookParking(slot);
			msg = { msg: 'General Parking Booked', parkingNo: slot };
		}
	} else {
		slot = await checkGeneralParking();
		if (slot !== 0) {
			//book general parking for general user
			let result = await bookParking(slot);
			msg = { msg: 'General Parking Booked', parkingNo: slot };
		} else {
			//no parking available
			msg = { msg: 'Sorry, No Parking Available' };
		}
	}

	return res.status(200).json(msg);
};

exports.registerArrival = async (req, res) => {
	let phone = req.params.phone;
	let booking = await BookingModel.findOne({ userPhone: phone });
	//check if user is late than designated arrival time
	let deadline = moment(booking.arrivalTime);
	let timeRemaining = deadline.diff(new Date(), 'seconds');
	msg = null;
	if (timeRemaining > 0) {
		let doc = await BookingModel.findOneAndUpdate(
			{ userPhone: phone },
			{ hasArrived: true }
		);
		msg = { msg: 'You are in Time, Welcome!', parkingNo: booking.parkingNo };
	} else {
		msg = {
			msg: 'You are late, please rebook another parking slot!',
			parkingNo: booking.parkingNo,
		};
	}
	res.status(200).json(msg);
};
