const express = require('express');
const router = express.Router();
const controller = require('../controllers/parking.controller');

router.get('/available', controller.listAvailableSlots);

router.get('/occupied', controller.listOccupiedSlots);

router.get('/users', controller.listRegisteredUsers);

/*
JSON Request:
{
    phone:"",
    firstName:"",
    lastName:"",
    gender:"",
    isPregnant:,
    isDiffabled:
}
*/

router.post('/register', controller.registerParkingSlot);
router.post('/arrive/:phone', controller.registerArrival);

module.exports = router;
