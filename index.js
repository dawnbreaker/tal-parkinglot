const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const port = process.env.PORT || 5001;
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');
const moment = require('moment');
const SlotModel = require('./models/slot.model');
const BookingModel = require('./models/slot.model');

const parkingRouter = require('./routes/parking.routes');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/parking', parkingRouter);

const initParkingSlots = async () => {
	for (let i = 1; i <= 120; i++) {
		let isReserved = i <= 24 ? true : false;
		let doc = await SlotModel.updateOne(
			{ parkingNo: i },
			{
				parkingNo: i,
				isAvailable: true,
				isReserved: isReserved,
			},
			{ upsert: true }
		);
	}
};

const parkingTimer = async () => {
	let allBookings = await BookingModel.find();
	allBookings.forEach(async (b) => {
		let deadline = moment(b.arrivalTime);
		let timeRemaining = deadline.diff(new Date(), 'seconds');
		if (timeRemaining <= 0) {
			//free the slot
			let doc3 = await SlotModel.findOneAndUpdate(
				{ parkingNo: b.parkingNo },
				{ isAvailable: true }
			);
		}
	});
	console.log('Parking scn completed at : ' + new Date());
};

//Scan all bookings for late arrivals every 60 seconds
//Free the parking slot if so
setInterval(parkingTimer, 60000);

mongoose.Promise = global.Promise;

mongoose
	.connect(dbConfig.url, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
	})
	.then(() => {
		console.log('Successfully connected to the database');
		console.log('Initializing Parking Slots...');
		initParkingSlots();
	})
	.catch((err) => {
		console.log('Could not connect to the database... Exiting now...', err);
		process.exit();
	});

app.listen(port, () => {
	console.log(`Parking-Lot Service is running on ${port}`);
});
